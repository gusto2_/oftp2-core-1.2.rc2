/**
 * Neociclo Accord, Open Source B2B Integration Suite
 * Copyright (C) 2005-2010 Neociclo, http://www.neociclo.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id: NormalizedVirtualFile.java 713 2010-09-22 21:01:12Z rmarins $
 */
package org.neociclo.odetteftp.protocol;

/**
 * @author Rafael Marins
 * @version $Rev: 713 $ $Date: 2010-09-22 23:01:12 +0200 (Wed, 22 Sep 2010) $
 */
public interface NormalizedVirtualFile extends VirtualFile {

	VirtualFile getOriginalVirtualFile();
}
