/**
 * Neociclo Accord, Open Source B2Bi Middleware
 * Copyright (C) 2005-2010 Neociclo, http://www.neociclo.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id: VirtualFile.java 558 2010-08-12 16:21:23Z rmarins $
 */
package org.neociclo.odetteftp.protocol;

import java.io.File;

/**
 * @author Rafael Marins
 * @version $Rev: 558 $ $Date: 2010-08-12 18:21:23 +0200 (Thu, 12 Aug 2010) $
 */
public interface VirtualFile extends OdetteFtpObject {

    RecordFormat getRecordFormat();

    int getRecordSize();
    
    long getSize();

    long getRestartOffset();

    File getFile();

}
