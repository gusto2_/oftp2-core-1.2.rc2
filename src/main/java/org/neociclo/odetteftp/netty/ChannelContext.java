/**
 * Neociclo Accord, Open Source B2Bi Middleware
 * Copyright (C) 2005-2010 Neociclo, http://www.neociclo.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id: ChannelContext.java 439 2010-06-29 22:06:10Z rmarins $
 */
package org.neociclo.odetteftp.netty;

import org.jboss.netty.channel.ChannelLocal;
import org.neociclo.odetteftp.OdetteFtpSession;

/**
 * @author Rafael Marins
 * @version $Rev: 439 $ $Date: 2010-06-30 00:06:10 +0200 (Wed, 30 Jun 2010) $
 */
public final class ChannelContext {

    public static final ChannelLocal<OdetteFtpSession> SESSION = new ChannelLocal<OdetteFtpSession>();

}
