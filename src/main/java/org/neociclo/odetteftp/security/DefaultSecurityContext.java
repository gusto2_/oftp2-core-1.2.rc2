/**
 * Neociclo Accord, Open Source B2B Integration Suite
 * Copyright (C) 2005-2010 Neociclo, http://www.neociclo.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id: DefaultSecurityContext.java 817 2010-10-20 23:33:38Z rmarins $
 */
package org.neociclo.odetteftp.security;

import javax.security.auth.callback.CallbackHandler;


/**
 * @author Rafael Marins
 * @version $Rev: 817 $ $Date: 2010-10-21 01:33:38 +0200 (Thu, 21 Oct 2010) $
 */
public class DefaultSecurityContext implements SecurityContext {

	private MappedCallbackHandler callbackHandler;

	public DefaultSecurityContext(MappedCallbackHandler callbackHandler) {
		super();
		this.callbackHandler = callbackHandler;
	}

	public CallbackHandler getCallbackHandler() {
		return callbackHandler;
	}

}
