/**
 * Neociclo Accord, Open Source B2Bi Middleware
 * Copyright (C) 2005-2010 Neociclo, http://www.neociclo.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id: ChannelCallback.java 461 2010-07-02 11:06:37Z rmarins $
 */
package org.neociclo.odetteftp.oftplet;

/**
 * @author Rafael Marins
 * @version $Rev: 461 $ $Date: 2010-07-02 13:06:37 +0200 (Fri, 02 Jul 2010) $
 */
public interface ChannelCallback {

    void close();

    void closeImmediately();

    void write(Object message, Runnable execOnComplete);

}
