/**
 * Neociclo Accord, Open Source B2B Integration Suite
 * Copyright (C) 2005-2010 Neociclo, http://www.neociclo.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id: ServerOftplet.java 805 2010-10-20 13:14:54Z rmarins $
 */
package org.neociclo.odetteftp.oftplet;


/**
 * @author Rafael Marins
 * @version $Rev: 805 $ $Date: 2010-10-20 15:14:54 +0200 (Wed, 20 Oct 2010) $
 */
public interface ServerOftplet extends Oftplet {

	void configure();

}
