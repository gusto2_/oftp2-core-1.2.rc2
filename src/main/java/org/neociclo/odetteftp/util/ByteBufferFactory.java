/**
 * Neociclo Accord, Open Source B2Bi Middleware
 * Copyright (C) 2005-2010 Neociclo, http://www.neociclo.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * $Id: ByteBufferFactory.java 430 2010-06-29 14:13:43Z rmarins $
 */
package org.neociclo.odetteftp.util;

import java.nio.ByteBuffer;

/**
 * @author Rafael Marins
 * @version $Rev: 430 $ $Date: 2010-06-29 16:13:43 +0200 (Tue, 29 Jun 2010) $
 */
public class ByteBufferFactory {

    public static ByteBuffer allocate(int size) {
        return ByteBuffer.allocate(size);
    }

}
